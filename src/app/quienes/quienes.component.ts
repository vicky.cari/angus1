import { Component, OnInit } from '@angular/core';
import { CssanimadoDirective} from '../cssanimado.directive';
@Component({
  selector: 'app-quienes',
  templateUrl: './quienes.component.html',
  styleUrls: ['./quienes.component.css']
})
export class QuienesComponent implements OnInit {

  decide: boolean = true;

  lista = [
    {name: 'lunes'},
    {name: 'martes'},
    {name: 'miercoles'},
    {name: 'jueves'},
    {name: 'viernes'},
    {name: 'sabado'},
    {name: 'domingo'},
  ];

  constructor() { }

  ngOnInit() { }
  public mostrar() {
    this.decide = !this.decide;
  }

  ngOnDestroy(): void {
    console.log('Se destruyo');
  }
}
