import { Component, OnInit } from '@angular/core';
import { MultiplicacionService } from '../multiplicacion.service';
@Component({
  selector: 'app-menuizquierdo',
  templateUrl: './menuizquierdo.component.html',
  styleUrls: ['./menuizquierdo.component.css']
})
export class MenuizquierdoComponent implements OnInit {

  constructor(public m: MultiplicacionService) { }

  ngOnInit() {
    this.m.division(8, 4);
    this.m.multiplicacion(2, 3);
  }

}
