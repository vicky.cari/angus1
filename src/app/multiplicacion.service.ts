import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MultiplicacionService {

  constructor() { }

  division(a: number, b: number) {
    console.log(a / b);
  }

  multiplicacion(a: number, b: number) {
    console.log(a * b);
  }
}
