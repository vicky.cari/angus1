import { TestBed } from '@angular/core/testing';

import { MultiplicacionService } from './multiplicacion.service';

describe('MultiplicacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MultiplicacionService = TestBed.get(MultiplicacionService);
    expect(service).toBeTruthy();
  });
});
