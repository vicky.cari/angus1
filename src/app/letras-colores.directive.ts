import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appLetrasColores]'
})
export class LetrasColoresDirective {

  constructor(col: ElementRef) {
    col.nativeElement.style.color = 'blue';
   }

}
