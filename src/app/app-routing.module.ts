import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuienesComponent} from './quienes/quienes.component';
import { InicioComponent} from './inicio/inicio.component';
import {ServiciosComponent} from './servicios/servicios.component';
import {AyudaComponent} from './ayuda/ayuda.component';

const routes: Routes = [
  {path: 'Inicio', component: InicioComponent},
  {path: 'Quienes', component: QuienesComponent},
  {path: 'servicios', component: ServiciosComponent},
  {path: 'ayuda', component: AyudaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
