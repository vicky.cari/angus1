import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CabeceraComponent } from './cabecera/cabecera.component';

import { MenuizquierdoComponent } from './menuizquierdo/menuizquierdo.component';
import { AdicionService } from './adicion.service';


import { MultiplicacionService } from './multiplicacion.service';
import { InicioComponent } from './inicio/inicio.component';
import { QuienesComponent } from './quienes/quienes.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { AyudaComponent } from './ayuda/ayuda.component';
import { ReemplazaRegexPipe } from './reemplaza-regex.pipe';
import { CssanimadoDirective } from './cssanimado.directive';
import { MiPipaPipe } from './mi-pipa.pipe';
import { LetrasColoresDirective } from './letras-colores.directive';



@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    MenuizquierdoComponent,
    InicioComponent,
    QuienesComponent,
    ServiciosComponent,
    AyudaComponent,
    ReemplazaRegexPipe,
    CssanimadoDirective,
    MiPipaPipe,
    LetrasColoresDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],

  providers: [AdicionService, MultiplicacionService],

  bootstrap: [AppComponent]
})
export class AppModule { }
