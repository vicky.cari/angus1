import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCssanimado]'
})
export class CssanimadoDirective {

  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'green';
  }

}
