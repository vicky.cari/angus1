import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'miPipa'
})
export class MiPipaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const resultado =  value.replace('hola' , '');
    return resultado;
  }
}
