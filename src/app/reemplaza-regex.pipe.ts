import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reemplazaRegex'
})
export class ReemplazaRegexPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.replace(new RegExp('xxx', 'g'), '');
  }

}
