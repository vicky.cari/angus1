import { Component, OnInit } from '@angular/core';
import {AdicionService} from '../adicion.service'; 

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {

  constructor(public a:AdicionService) { }

  ngOnInit() {
    this.a.suma(2,2);
    this.a.resta(6,3);
  }

}
