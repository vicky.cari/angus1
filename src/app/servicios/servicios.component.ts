import { Component, OnInit } from '@angular/core';
import {MiPipaPipe} from '../mi-pipa.pipe';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})


export class ServiciosComponent implements OnInit {
 titulo = 'hola MUNDO';
  monto = 34.221122;
  res = 'hola a todos';
  info = false;
  lista: any = [
              {id: 'lunea', n: '1'},
              {id: 'martes', n: '2'},
              {id: 'miercoles', n: '3'}
          ];

  constructor() { }

  ngOnInit() {  }

  cambiar() { this.info = !this.info; }

  ngOnDestroy() {
    console.log('ngOnDestroy');
  }
  
}
